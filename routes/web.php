<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

// Auth::routes();

// Route::get('/home', 'HomeController@index');
Route::get('dafinci', 'ApiController@dafinci');
Route::get('remedy', 'ApiController@remedy');
Route::get('btsonair', 'ApiController@btsonair');
Route::get('nodin', 'ApiController@nodin');
Route::get('add_license', 'ApiController@add_license');
Route::get('act_cell', 'ApiController@act_cell');
Route::get('dea_cell', 'ApiController@dea_cell');
Route::get('act_license', 'ApiController@act_license');
Route::get('dea_license', 'ApiController@dea_license');
Route::get('chart_btsonair', 'ApiController@chart_btsonair');
Route::get('chart_nodin', 'ApiController@chart_nodin');
Route::get('chart_user', 'ApiController@chart_user');
Route::get('chart_dea', 'ApiController@chart_dea');
Route::get('add_bts', 'ApiController@add_bts');
Route::get('cell_deactivated', 'ApiController@cell_deactivated');
Route::get('test', 'ApiController@test');

Route::get('plan_remedy', 'ApiController@plan_remedy');
Route::get('plan_onair', 'ApiController@plan_onair');

