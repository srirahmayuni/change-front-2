<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Project Tracking</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{url('')}}/tsel.jpg" rel="icon" type="image/png">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('')}}/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{url('')}}/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{url('')}}/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{url('')}}/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{url('')}}/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{url('')}}/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
         <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
        </li>
        <li class="nav-item navbar-brand" style="color:white;">Project Tracking</li>
        <li class="nav-item">
          <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                  color: #000;
                  position: absolute;
                  font-size: 10px;
                  right: 6%;
                  height: auto;
                  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                  text-transform: uppercase;
                  font-family: Roboto;
                  padding: 1%;"><i class="fa fa-sign-out"></i> Log Out</a>
        </li>
      </ul>
    </nav>

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
        <img src="{{url('')}}/dist/img/tsel-white.png"
             style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
      </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-bts-on-air/public" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-license" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>License</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/btsonair" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>BTS On Air</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Create Integrasi</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Create Rehoming</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Create Dismantle</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Create Relocation</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-swap/index.php/NodinController" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Create Swap</p>
                            </a>
                        </li>
                        <li class="nav-item" style="background: #d81b60;">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Project Tracking</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/tableList" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Table List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link">
                                <i class="nav-icon fa fa-fire"></i>
                                <p>Report Remedy</p>
                            </a>
                        </li>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-top:60px; min-height: 553px;">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/index">Regional 1</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Regional 2</li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_3">Regional 3</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_4">Regional 4</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_5">Regional 5</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_6">Regional 6</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_7">Regional 7</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_8">Regional 8</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_9">Regional 9</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_10">Regional 10</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="http://10.54.36.49/change-front-2/public/regional_11">Regional 11</a></li>
                    </ol>
                </nav>

                <section class="content">
                    <div class="container-fluid">
                        <!-- Small boxes (Stat box) -->
                        <div class="row">
                            <div class="col-sm-12 mt-3">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2">
                                        <!-- small box -->
                                        <div class="small-box bg-info">
                                            <div class="inner">
                                                <div class="col-xs-12 text-right">
                                                    <div class="huge" style="font-size:40px;" id="dafinci">0</div>
                                                    <div style="font-size:16px;">Site</div>
                                                </div>
                                                <span class="pull-left" style="font-size:20px;" id="dafinci_change">Dafinci&nbsp;<i class="fa" style="color:green">&#xf062;</i>0</span>
                                                <div class="clearfix"></div>
                                            </div>
                                            <!-- <div class="icon">
                                            <i class="ion ion-bag"></i>
                                        </div> -->
                                        <!---<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                                    </div>
                                </div>
                                <!-- ./col -->
                                <div class="col-lg-2 col-md-2">
                                    <!-- small box -->
                                    <div class="small-box bg-warning">
                                        <div class="inner">
                                            <div class="col-xs-12 text-right">
                                                <div class="huge" style="font-size:40px;" id="nodin">0</div>
                                                <div style="font-size:16px;">Site</div>
                                            </div>
                                            <span class="pull-left" style="font-size:20px;" id="nodin_change">Nodin&nbsp;<i class="fa" style="color:green">&#xf062;</i>0</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!--<div class="icon">
                                        <i class="ion ion-stats-bars"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-2 col-md-2">
                                <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <div class="col-xs-12 text-right">
                                            <div class="huge" style="font-size:40px;" id="remedy">0</div>
                                            <div style="font-size:16px;">Site</div>
                                        </div>
                                        <span class="pull-left" style="font-size:20px;" id="remedy_change">Remedy&nbsp;<i class="fa" style="color:green">&#xf062;</i>0</span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!--<div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-md-2">
                            <!-- small box -->
                            <div class="small-box" style="background-color: #FF8C00;">
                                <div class="inner">
                                    <div class="col-xs-12 text-right">
                                        <div class="huge" style="font-size:40px;" id="add_bts">0</div>
                                        <div style="font-size:16px;">Site</div>
                                    </div>
                                    <span class="pull-left" style="font-size:20px;" id="change_add_bts">Change&nbsp;<i class="fa" style="color:green">&#xf062;</i>0</span>
                                    <div class="clearfix"></div>
                                </div>
                                <!--<div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-2 col-md-2">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <div class="col-xs-12 text-right">
                                    <div class="huge" style="font-size:40px;" id="license">0</div>
                                    <div style="font-size:16px;">Site</div>
                                </div>
                                <span class="pull-left" style="font-size:20px;" id="license_change">License&nbsp;<i class="fa" style="color:green">&#xf062;</i>0</span>
                                <div class="clearfix"></div>
                            </div>
                            <!--<div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div>
                </div>

                <div class="col-lg-2 col-md-2">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <div class="col-xs-12 text-right">
                                <div class="huge" style="font-size:40px;" id="btsonair">0</div>
                                <div style="font-size:16px;">Site</div>
                            </div>
                            <span class="pull-left" style="font-size:20px;" id="btsonair_change">On Air&nbsp;<i class="fa" style="color:green">&#xf062;</i>0</span>
                            <div class="clearfix"></div>
                        </div>
                        <!--<div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
        </div>
    </div>
    <section class="col-lg-6">
        <div class="card">
            <div class="card-header" style="background-color: #008080;">
                <h3 style="color: white;" class="card-title">
                    <i class="fa fa-th mr-1"></i>
                    Count of Planned and Remedy (Region)
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" style="background-color: #343a40;">
                <center><label class="pull-center" style="color:white;">Planned &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;<label class="pull-center" style="color:white;">Remedy &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button></center>
                @include('plan_remedy')
            </div>
        </section>

        <section class="col-lg-6">
            <div class="card">
                <div class="card-header" style="background-color: #008080;">
                    <h3 style="color: white;" class="card-title">
                        <i class="fa fa-th mr-1"></i>
                        Count of Planned and On Air (Region)
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-widget="remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body" style="background-color: #343a40;">
                    <center><label class="pull-center" style="color:white;">Planned &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;<label class="pull-center" style="color:white;">On Air &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button></center>
                    @include('plan_onair')
                </div>
            </section>

            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="small-box" style="height: 270px; background-color: #343a40">
                            <div class="inner">
                                <div class="col-xs-12 text-center">
                                    <div class="huge ccll"  style="font-size:24px;" id="nodin"><h4 style="color:white">Activated Cell</h4></div>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <div class="huge ccval" style="font-size:40px;" id="act_cell"><h1 style="color:white">0</h1></div>
                                    <br/>
                                    <br/>
                                    <div class="huge ccll" style="font-size:20px;"><h4 style="color:white">Cell</h4></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3">
                        <!-- small box -->
                        <div class="small-box" style="height: 270px; background-color: #343a40">
                            <div class="inner">
                                <div class="col-xs-12 text-center">
                                    <div class="huge ccli" style="font-size:24px;" id="nodin"><h4 style="color:white">Deactivated Cell</h4></div>
                                    <br/>
                                    <br/>
                                    <div class="huge ccval" style="font-size:40px;" id="dea_cell"><h1 style="color:white">0</h1></div>
                                    <br/>
                                    <br/>
                                    <div class="huge" style="font-size:20px;"><h4 style="color:white">Cell</h4></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3">
                        <!-- small box -->
                        <div class="small-box" style="height: 270px; background-color: #343a40">
                            <div class="inner">
                                <div class="col-xs-12 text-center">
                                    <div class="huge" style="font-size:24px;" id="nodin"><h4 style="color:white">Active License</h4></div>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <div class="huge ccval" style="font-size:40px;" id="act_license"><h1 style="color:white">0</h1></div>
                                    <br/>
                                    <br/>
                                    <div class="huge" style="font-size:20px;"><h4 style="color:white">License</h4></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <!-- ./col -->
                    <div class="col-lg-3">
                        <div class="small-box" style="height: 270px; background-color: #343a40">
                            <div class="inner">
                                <div class="col-xs-12 text-center">
                                    <div class="huge" style="font-size:24px;" id="nodin"><h4 style="color:white">Deactivated License</h4></div>
                                    <br/>
                                    <br/>
                                    <div class="huge ccval" style="font-size:40px;" id="dea_license"><h1 style="color:white">0</h1></div>
                                    <br/>
                                    <br/>
                                    <div class="huge" style="font-size:20px;"><h4 style="color:white">License</h4></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="col-lg-6">
                <div class="card">
                    <div class="card-header" style="background-color: #008080;">
                        <h3 style="color: white;" class="card-title">
                            <i class="fa fa-th mr-1"></i>
                            Count of BTS On Air (Region)
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="background-color: #343a40;">
                        @include('top_5_chart_monly')
                    </div>
                </section>
                </div>
            </div>
        </div>

        <footer class="main-footer">
            <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
            <div class="float-right d-none d-sm-inline-block">
                <b>CHANGE
                </div>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <!-- jQuery -->
        <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script src="{{url('')}}/js/jquery-ui.min.js"></script>
        <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{url('')}}/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="{{url('')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{url('')}}/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{url('')}}/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="{{url('')}}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="{{url('')}}/dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{url('')}}/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{url('')}}/dist/js/demo.js"></script>
    <script>
    $("#nav-open").click(function(){
        if (document.getElementById("wrapper").style.padding == "0px") {
            $("#wrapper").css('padding-left', '150px');
            $("#sidebar-wrapper").css('width', '150px');
        }else {
            document.getElementById("wrapper").style.padding = "0";
            document.getElementById("sidebar-wrapper").style.width = "0";
        }
    });

    $(document).ready(function(){
        $.ajax({
            url:'{{url("dafinci2")}}',
            type:'get',
            success: function(result){
                $('#dafinci').html(result);
                $('#dafinci_change').html('Dafinci&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'{{url("remedy2")}}',
            type:'get',
            success: function(result){
                $('#remedy').html(result);
                $('#remedy_change').html('Remedy&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'{{url("add_bts2")}}',
            type:'get',
            success: function(result){
                $('#add_bts').html(result);
                $('#change_add_bts').html('Change&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'{{url("btsonair2")}}',
            type:'get',
            success: function(result){
                $('#btsonair').html(result);
                $('#btsonair_change').html('On Air&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'{{url("nodin2")}}',
            type:'get',
            success: function(result){
                $('#nodin').html(result);
                $('#nodin_change').html('Nodin&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });

        $.ajax({
            url:'{{url("act_cell2")}}',
            type:'get',
            success: function(result){
                $('#act_cell').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'{{url("dea_cell2")}}',
            type:'get',
            success: function(result){
                $('#dea_cell').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'{{url("act_license2")}}',
            type:'get',
            success: function(result){
                $('#act_license').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'{{url("dea_license2")}}',
            type:'get',
            success: function(result){
                $('#dea_license').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'{{url("add_license2")}}',
            type:'get',
            success: function(result){
                $('#license').html(result);
                $('#license_change').html('License&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });

    });
</script>
</body>
</html>
