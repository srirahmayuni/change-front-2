<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    const URL = 'http://10.54.36.49/change2/public/';

    static function add_bts()
    {
        $url = self::URL.'add_bts';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    
    static function top_5_user_activity()
    {
        $url = self::URL.'top_5_user_activity';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function user_activity()
    {
        $url = self::URL.'user_activity';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function mml_command($command)
    {
        $url = self::URL.'mml_command/'.$command;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated()
    {
        $url = self::URL.'license_activated';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function cell_deactivated()
    {
        $url = self::URL.'cell_deactivated';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function dafinci()
    {
        $url = self::URL.'dafinci';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function change()
    {
        $url = self::URL.'change';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function remedy()
    {
        $url = self::URL.'remedy';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function btsonair()
    {
        $url = self::URL.'btsonair';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function nodin()
    {
        $url = self::URL.'nodin';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function chart_btsonair()
    {
        $url = self::URL.'chart_btsonair';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_nodin()
    {
        $url = self::URL.'chart_nodin';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    static function chart_user()
    {
        $url = self::URL.'chart_user';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_dea()
    {
        $url = self::URL.'chart_dea';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell()
    {
        $url = self::URL.'act_cell';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }
    static function dea_cell()
    {
        $url = self::URL.'dea_cell';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }
     static function add_license()
    {
        $url = self::URL.'add_license';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function act_license()
    {
        $url = self::URL.'act_license';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license()
    {
        $url = self::URL.'dea_license';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    //change2

    static function plan_remedy()
    {
        $url = self::URL.'plan_remedy';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }    
    static function plan_onair()
    {
        $url = self::URL.'plan_onair';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

}
