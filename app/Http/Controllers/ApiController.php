<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;

class ApiController extends Controller
{
    public function top_5_user_activity()
    {
        $get = Api::top_5_user_activity();

        return $get;
    }

    public function user_activity()
    {
        $get = Api::user_activity();

        return $get;
    }

    public function mml_command(Request $request)
    {
        $get = Api::mml_command($request->input('command'));

        return $get;
    }

    //dafinci 1-11
    public function dafinci()
    {
        $data = Api::dafinci();

        return $data;
    }

    public function dafinci2()
    {
        $data = Api::dafinci2();

        return $data;
    }

    public function dafinci3()
    {
        $data = Api::dafinci3();

        return $data;
    }

    public function dafinci4()
    {
        $data = Api::dafinci4();

        return $data;
    }

    public function dafinci5()
    {
        $data = Api::dafinci5();

        return $data;
    }

    public function dafinci6()
    {
        $data = Api::dafinci6();

        return $data;
    }

    public function dafinci7()
    {
        $data = Api::dafinci7();

        return $data;
    }

    public function dafinci8()
    {
        $data = Api::dafinci8();

        return $data;
    }

    public function dafinci9()
    {
        $data = Api::dafinci9();

        return $data;
    }

    public function dafinci10()
    {
        $data = Api::dafinci10();

        return $data;
    }

    public function dafinci11()
    {
        $data = Api::dafinci11();

        return $data;
    }    
    //

    //remedy 1-11
    public function remedy()
    {
        $data = Api::remedy();

        return $data;
    }

    public function remedy2()
    {
        $data = Api::remedy2();

        return $data;
    }

    public function remedy3()
    {
        $data = Api::remedy3();

        return $data;
    }

    public function remedy4()
    {
        $data = Api::remedy4();

        return $data;
    }

    public function remedy5()
    {
        $data = Api::remedy5();

        return $data;
    }

    public function remedy6()
    {
        $data = Api::remedy6();

        return $data;
    }

    public function remedy7()
    {
        $data = Api::remedy7();

        return $data;
    }

    public function remedy8()
    {
        $data = Api::remedy8();

        return $data;
    }

    public function remedy9()
    {
        $data = Api::remedy9();

        return $data;
    }

    public function remedy10()
    {
        $data = Api::remedy10();

        return $data;
    }

    public function remedy11()
    {
        $data = Api::remedy11();

        return $data;
    }
    //

    //btsonair 1-11
    public function btsonair()
    {
        $data = Api::btsonair();

        return $data;
    }

    public function btsonair2()
    {
        $data = Api::btsonair2();

        return $data;
    }

    public function btsonair3()
    {
        $data = Api::btsonair3();

        return $data;
    }

    public function btsonair4()
    {
        $data = Api::btsonair4();

        return $data;
    }

    public function btsonair5()
    {
        $data = Api::btsonair5();

        return $data;
    }

    public function btsonair6()
    {
        $data = Api::btsonair6();

        return $data;
    }

    public function btsonair7()
    {
        $data = Api::btsonair7();

        return $data;
    }

    public function btsonair8()
    {
        $data = Api::btsonair8();

        return $data;
    }

    public function btsonair9()
    {
        $data = Api::btsonair9();

        return $data;
    }

    public function btsonair10()
    {
        $data = Api::btsonair10();

        return $data;
    }

    public function btsonair11()
    {
        $data = Api::btsonair11();

        return $data;
    }
    //

    //nodin 1-11
    public function nodin()
    {
        $data = Api::nodin();

        return $data;
    }

    public function nodin2()
    {
        $data = Api::nodin2();

        return $data;
    }

    public function nodin3()
    {
        $data = Api::nodin3();

        return $data;
    }

    public function nodin4()
    {
        $data = Api::nodin4();

        return $data;
    }

    public function nodin5()
    {
        $data = Api::nodin5();

        return $data;
    }

    public function nodin6()
    {
        $data = Api::nodin6();

        return $data;
    }

    public function nodin7()
    {
        $data = Api::nodin7();

        return $data;
    }

    public function nodin8()
    {
        $data = Api::nodin8();

        return $data;
    }

    public function nodin9()
    {
        $data = Api::nodin9();

        return $data;
    }

    public function nodin10()
    {
        $data = Api::nodin10();

        return $data;
    }

    public function nodin11()
    {
        $data = Api::nodin11();

        return $data;
    }
    //

    public function chart_btsonair()
    {
        $data = Api::chart_btsonair();

        return $data;
    }

    public function chart_nodin()
    {
        $data = Api::chart_nodin();

        return $data;
    }

    public function chart_user()
    {
        $data = Api::chart_user();

        return $data;
    }

    public function chart_dea()
    {
        $data = Api::chart_dea();

        return $data;
    }

    //active cell 1-11
    public function act_cell(){
        $data = Api::act_cell();
        return $data;
    }

    public function act_cell2(){
        $data = Api::act_cell2();
        return $data;
    }

    public function act_cell3(){
        $data = Api::act_cell3();
        return $data;
    }

    public function act_cell4(){
        $data = Api::act_cell4();
        return $data;
    }

    public function act_cell5(){
        $data = Api::act_cell5();
        return $data;
    }

    public function act_cell6(){
        $data = Api::act_cell6();
        return $data;
    }

    public function act_cell7(){
        $data = Api::act_cell7();
        return $data;
    }    

    public function act_cell8(){
        $data = Api::act_cell8();
        return $data;
    }

    public function act_cell9(){
        $data = Api::act_cell9();
        return $data;
    }

    public function act_cell10(){
        $data = Api::act_cell10();
        return $data;
    }

    public function act_cell11(){
        $data = Api::act_cell11();
        return $data;
    }
    //

    //deactive cell 1-11
    public function dea_cell(){
        $data = Api::dea_cell();
        return $data;
    }

    public function dea_cell2(){
        $data = Api::dea_cell2();
        return $data;
    }

    public function dea_cell3(){
        $data = Api::dea_cell3();
        return $data;
    }

    public function dea_cell4(){
        $data = Api::dea_cell4();
        return $data;
    }

    public function dea_cell5(){
        $data = Api::dea_cell5();
        return $data;
    }

    public function dea_cell6(){
        $data = Api::dea_cell6();
        return $data;
    }

    public function dea_cell7(){
        $data = Api::dea_cell7();
        return $data;
    }

    public function dea_cell8(){
        $data = Api::dea_cell8();
        return $data;
    }

    public function dea_cell9(){
        $data = Api::dea_cell9();
        return $data;
    }

    public function dea_cell10(){
        $data = Api::dea_cell10();
        return $data;
    }

    public function dea_cell11(){
        $data = Api::dea_cell11();
        return $data;
    }
    //

    //license 1-11
    public function add_license(){
        $data = Api::add_license();
        return $data;
    }

    public function add_license2(){
        $data = Api::add_license2();
        return $data;
    }

    public function add_license3(){
        $data = Api::add_license3();
        return $data;
    }

    public function add_license4(){
        $data = Api::add_license4();
        return $data;
    }

    public function add_license5(){
        $data = Api::add_license5();
        return $data;
    }

    public function add_license6(){
        $data = Api::add_license6();
        return $data;
    }

    public function add_license7(){
        $data = Api::add_license7();
        return $data;
    }

    public function add_license8(){
        $data = Api::add_license8();
        return $data;
    }

    public function add_license9(){
        $data = Api::add_license9();
        return $data;
    }

    public function add_license10(){
        $data = Api::add_license10();
        return $data;
    }

    public function add_license11(){
        $data = Api::add_license11();
        return $data;
    }
    //

    //active license 1-11
    public function act_license(){
        $data = Api::act_license();
        return $data;
    }

    public function act_license2(){
        $data = Api::act_license2();
        return $data;
    }

    public function act_license3(){
        $data = Api::act_license3();
        return $data;
    }

    public function act_license4(){
        $data = Api::act_license4();
        return $data;
    }

    public function act_license5(){
        $data = Api::act_license5();
        return $data;
    }

    public function act_license6(){
        $data = Api::act_license6();
        return $data;
    }

    public function act_license7(){
        $data = Api::act_license7();
        return $data;
    }

    public function act_license8(){
        $data = Api::act_license8();
        return $data;
    }

    public function act_license9(){
        $data = Api::act_license9();
        return $data;
    }

    public function act_license10(){
        $data = Api::act_license10();
        return $data;
    }

    public function act_license11(){
        $data = Api::act_license11();
        return $data;
    }
    //

    //deactive license
    public function dea_license(){
        $data = Api::dea_license();
        return $data;
    }

    public function dea_license2(){
        $data = Api::dea_license2();
        return $data;
    }

    public function dea_license3(){
        $data = Api::dea_license3();
        return $data;
    }

    public function dea_license4(){
        $data = Api::dea_license4();
        return $data;
    }

    public function dea_license5(){
        $data = Api::dea_license5();
        return $data;
    }

    public function dea_license6(){
        $data = Api::dea_license6();
        return $data;
    }

    public function dea_license7(){
        $data = Api::dea_license7();
        return $data;
    }

    public function dea_license8(){
        $data = Api::dea_license8();
        return $data;
    }

    public function dea_license9(){
        $data = Api::dea_license9();
        return $data;
    }

    public function dea_license10(){
        $data = Api::dea_license10();
        return $data;
    }

    public function dea_license11(){
        $data = Api::dea_license11();
        return $data;
    }
    //

    //change 1-11
    public function add_bts(){
        $data = Api::add_bts();
        return $data;
    }

    public function add_bts2(){
        $data = Api::add_bts2();
        return $data;
    }

    public function add_bts3(){
        $data = Api::add_bts3();
        return $data;
    }

    public function add_bts4(){
        $data = Api::add_bts4();
        return $data;
    }

    public function add_bts5(){
        $data = Api::add_bts5();
        return $data;
    }

    public function add_bts6(){
        $data = Api::add_bts6();
        return $data;
    }

    public function add_bts7(){
        $data = Api::add_bts7();
        return $data;
    }

    public function add_bts8(){
        $data = Api::add_bts8();
        return $data;
    }

    public function add_bts9(){
        $data = Api::add_bts9();
        return $data;
    }

    public function add_bts10(){
        $data = Api::add_bts10();
        return $data;
    }

    public function add_bts11(){
        $data = Api::add_bts11();
        return $data;
    }
    //

    public function cell_deactivated(){
        $data = Api::cell_deactivated();
        return $data;
    }

    //change2

    public function plan_remedy(){
        $data = Api::plan_remedy();
        return $data;
    }
    public function plan_onair(){
        $data = Api::plan_onair();
        return $data;
    }

}
